import React from "react";
import { connect } from 'react-redux'
import { RootState, getAuth } from "../../stores/rootReducer";
import { View, Alert } from "react-native";
import Spinner from "react-native-loading-spinner-overlay";
import { AuthState } from "../../stores/auth/types";
import { StackNavigationProp } from "@react-navigation/stack";
import { MainNavParamList } from "../Main";
import { login } from "../../stores/auth/actions";
import { Button, Form, Item, Input, Label, Text, Card, Container, Content, Icon } from 'native-base';
import { BgOvalLarge } from "../_components/BgOval";
import Ionicons from 'react-native-vector-icons/Ionicons';

interface OwnProps {
    navigation: StackNavigationProp<MainNavParamList>
}
interface StateProps extends AuthState { }
interface DispatchProps {
    onLogin: (email: string, password: string) => void
}
type Props = OwnProps & StateProps & DispatchProps

interface State {
    email?: string
    password?: string
}

class Login extends React.Component<Props, State> {

    state: State = {
        email: undefined,
        password: undefined
    }

    render() {
        const { status } = this.props
        const isLoading = status == 'pending'
        return (
            <>
                {isLoading && <Spinner visible={isLoading} />}
                <Container style={{ backgroundColor: 'white' }}>
                    <BgOvalLarge />
                    <Content scrollEnabled={false}
                        contentContainerStyle={{ flex: 1, paddingHorizontal: 32, marginTop: 132 }}>
                        <Ionicons
                            name={'ios-analytics'}
                            size={140}
                            color={'rgb(221,16,120)'}
                            style={{ alignSelf: "center", paddingBottom: 32 }} />
                        <Card style={{ paddingVertical: 32, paddingHorizontal: 16, borderRadius: 16 }}>
                            <Form>
                                <Item rounded style={{ paddingHorizontal: 8, marginBottom: 12 }}>
                                    <Icon name='person' />
                                    <Input
                                        placeholder={'enter email'}
                                        placeholderTextColor={'rgba(0,0,0,0.15)'}
                                        onChangeText={email => this.setState({ email })}
                                        value={this.state.email} />
                                </Item>
                                <Item rounded style={{ paddingHorizontal: 8 }}>
                                    <Icon name='lock' />
                                    <Input
                                        placeholder={'enter password'}
                                        placeholderTextColor={'rgba(0,0,0,0.15)'}
                                        onChangeText={password => this.setState({ password })}
                                        secureTextEntry={true}
                                        value={this.state.password} />
                                </Item>
                                <Button block rounded style={{ alignSelf: "center", width: 140, marginTop: 32 }}
                                    onPress={() => this.submitDidPressed()}>
                                    <Text>SIGN IN</Text>
                                </Button>
                                <Button block transparent small style={{ alignSelf: "center", width: 140, marginTop: 16 }}
                                    onPress={() => this.skipDidPressed()}>
                                    <Text>Skip</Text>
                                </Button>
                            </Form>
                        </Card>
                    </Content>
                </Container>
            </>
        )
    }

    submitDidPressed() {
        const { onLogin } = this.props
        if (this.state.email && this.state.password) {
            onLogin(this.state.email, this.state.password)
        } else {
            Alert.alert('Masukkan data login')
        }
    }

    skipDidPressed() {
        this.props.navigation.replace('HomeDrawer')
    }

    componentDidUpdate(prevProps: Props) {
        const { error } = this.props
        if (error && prevProps.error !== error) {
            setTimeout(() => {
                Alert.alert(error)
            }, 300)
        }
    }

}

const mapStateToProps = (state: RootState): StateProps => getAuth(state)

const mapDispatchToProps = (dispatch: any): DispatchProps => {
    return {
        onLogin: (email, password) => dispatch(login({ email, password })),
    }
}

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login)

export default LoginContainer
