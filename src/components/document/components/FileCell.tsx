import React from 'react'
import { styles } from '../../../styles'
import { View, Text, Image, Button } from 'react-native'
import { UploadFile } from '../../../api/types'

interface Props {
    data?: UploadFile
    onPressChoose?: (data?: UploadFile) => void
    onPressView?: (data?: UploadFile) => void
}

const FileCell: React.FC<Props> = ({ data, onPressChoose, onPressView }) => {
    return (
        <View style={[styles.cell]}>
            <View>
                <Text>{data?.filename || 'Pilih'}</Text>
                <Text>{data?.type || '-'}</Text>
                <Text>{data?.uri || '-'}</Text>
            </View>
            <View style={{ borderTopColor: 'grey', flexDirection: "row", justifyContent: "space-evenly" }}>
                <Button title={'Choose'} onPress={() => (onPressChoose ? onPressChoose(data) : () => (null))} />
                <Button title={'View'} onPress={() => (onPressView ? onPressView(data) : () => (null))} />
            </View>
        </View>
    )
}

export default FileCell
