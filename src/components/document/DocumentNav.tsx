import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { MainNavParamList } from "../Main";
import DocumentContainer from "./Document";
import { HeaderLeftView } from "../_components/headers/HeaderLeftView";
import { BackButton } from "../_components/headers/buttons/BackButton";

const Nav = createStackNavigator<MainNavParamList>()

const DocumentNav: React.FC = () => {
    return (
        <Nav.Navigator screenOptions={
            ({ navigation }) => ({
                headerTransparent: true,
                headerTintColor: 'white',
                headerLeft: (props) => (
                    <HeaderLeftView>
                        <BackButton {...props} onPress={() => (navigation.goBack())} />
                    </HeaderLeftView>
                )
            })
        }>
            <Nav.Screen name='Document' component={DocumentContainer} />
        </Nav.Navigator>
    )
}

export default DocumentNav
