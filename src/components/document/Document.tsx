import React from "react";
import { connect } from 'react-redux'
import { RootState } from "../../stores/rootReducer";
import { View } from "react-native";
import { StackNavigationProp } from "@react-navigation/stack";
import { UploadFile } from "../../api/types";
import { BgOvalLarge } from "../_components/BgOval";
import { MainNavParamList } from "../Main";
import FileCell from "./components/FileCell";
import DocumentPicker from 'react-native-document-picker';
import FileViewer from 'react-native-file-viewer';

interface OwnProps {
    navigation: StackNavigationProp<MainNavParamList>
}
interface StateProps { }
interface DispatchProps { }

type Props = OwnProps & StateProps & DispatchProps

interface State {
    allFile?: UploadFile
    pdfFile?: UploadFile
}

class Document extends React.Component<Props, State> {

    state: State = {
        allFile: undefined,
        pdfFile: undefined
    }

    render() {
        return (
            <>
                <BgOvalLarge />
                {this.renderBody()}
            </>
        )
    }

    renderBody() {
        const { allFile, pdfFile } = this.state
        return (
            <View style={{ flex: 1, marginTop: 100 }}>
                <FileCell
                    data={allFile}
                    onPressChoose={(file) => this.onPressAllFileCell(file)}
                    onPressView={(file) => file && this.openFile(file)}
                />
                <FileCell
                    data={pdfFile}
                    onPressChoose={(file) => this.onPressPdfFileCell(file)}
                    onPressView={(file) => file && this.openFile(file)}
                />
            </View>
        )
    }

    onPressAllFileCell(file?: UploadFile) {
        DocumentPicker.pick({ type: [DocumentPicker.types.allFiles] })
            .then(response => {
                console.log(
                    response.uri,
                    response.type, // mime type
                    response.name,
                    response.size
                )
                this.setState({
                    allFile: new UploadFile(response.uri, response.type, response.name)
                })
            })
            .catch(error => {
                console.log(error)
                if (DocumentPicker.isCancel(error)) {
                    //
                }
            })
    }

    onPressPdfFileCell(file?: UploadFile) {
        DocumentPicker.pick({ type: [DocumentPicker.types.pdf] })
            .then(response => {
                console.log(
                    response.uri,
                    response.type, // mime type
                    response.name,
                    response.size
                )
                this.setState({
                    pdfFile: new UploadFile(response.uri, response.type, response.name)
                })
            })
            .catch(error => {
                console.log(error)
                if (DocumentPicker.isCancel(error)) {
                    //
                }
            })
    }

    openFile(file: UploadFile) {
        const uri = file.uri.replace('file://', '')
        console.log('openFile : ' + uri);
        FileViewer.open(uri, { showOpenWithDialog: true })
            .then(() => {
                console.log('Success')
            })
            .catch(error => {
                console.log(error)
            });
    }

}

const mapStateToProps = (state: RootState): StateProps => ({})

const mapDispatchToProps = (dispatch: any): DispatchProps => ({})

const DocumentContainer = connect(mapStateToProps, mapDispatchToProps)(Document)

export default DocumentContainer
