import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react';
import LoadingView from "./_components/LoadingView";
import MainContainer from './Main';

const App = ({ store, persistor }: any) => (
    <Provider store={store}>
        <PersistGate loading={<LoadingView />} persistor={persistor}>
            <MainContainer />
        </PersistGate>
    </Provider>
)

export default App
