import React from "react";
import { connect } from 'react-redux'
import { RootState } from "../../stores/rootReducer";
import { Text, View, Button, Image, Alert, TouchableOpacity, ActivityIndicator } from "react-native";
import Spinner from 'react-native-loading-spinner-overlay';
import { StackNavigationProp } from "@react-navigation/stack";
import * as pref from "../../utilities/preference";
import { showImagePicker } from "../../utilities/mediaPicker";
import { UploadFile } from "../../api/types";
import Ionicons from 'react-native-vector-icons/Ionicons';
import { BgOvalLarge } from "../_components/BgOval";
import { MainNavParamList } from "../Main";
import { AuthState } from "../../stores/auth/types";
import { UserState } from "../../stores/user/types";
import { updateUserAvatar } from "../../stores/user/actions";
import { logout } from "../../stores/auth/actions";

interface OwnProps {
    navigation: StackNavigationProp<MainNavParamList>
}
interface StateProps extends UserState {
    auth: AuthState,
}
interface DispatchProps {
    onAvatarChanged: (picture: UploadFile) => void
    onLogout: (token: string) => void
}

type Props = OwnProps & StateProps & DispatchProps

interface State {
    avatar?: UploadFile
}

class Profile extends React.Component<Props, State> {

    state: State = {
        avatar: undefined
    }

    render() {
        const { status } = this.props.auth
        const isLoading = status == 'pending'
        return (
            <>
                {isLoading && <Spinner visible={isLoading} />}
                <BgOvalLarge />
                {this.renderBody()}
            </>
        )
    }

    renderBody() {
        const user = this.props.user
        const { status } = this.props
        const isLoading = status == 'pending'
        return (
            <View style={{ flex: 1, marginTop: 132, alignItems: "center" }}>
                {user == undefined
                    ? <>
                        <Text>No profile</Text>
                        <Button
                            title='Tes'
                            onPress={() => this.onLogoutPressed()} />
                    </>
                    : <>
                        <TouchableOpacity
                            style={{ marginBottom: 8 }}
                            onPress={() => this.onAvatarPressed()} >
                            <Image
                                style={{ width: 80, height: 80, borderRadius: 40, opacity: isLoading ? 0.1 : 1 }}
                                source={{ uri: this.state.avatar?.uri || user.pictureThumbnail }} />
                            <Ionicons style={{ position: "absolute", right: -4, bottom: -10 }} name={'ios-camera'} size={24} color={'tomato'} />
                            {isLoading
                                ? <ActivityIndicator style={{ position: "absolute", alignSelf: "center", height: 80 }} />
                                : null
                            }
                        </TouchableOpacity>
                        <Text style={{ marginBottom: 8 }}>{user.name}</Text>
                        <Button
                            title='Logout'
                            onPress={() => this.onLogoutPressed()} />
                        <Button
                            title='Document'
                            onPress={() => this.onPressDocument()} />
                    </>
                }
            </View>
        )
    }

    onAvatarPressed() {
        const { token } = this.props.auth
        const { status } = this.props
        const isLoading = status == 'pending'
        if (token == undefined || isLoading) {
            return
        }

        const { onAvatarChanged } = this.props
        showImagePicker((response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const avatar = new UploadFile(response.uri, response.type, response.fileName)
                this.setState({ avatar: avatar }, () => {
                    onAvatarChanged(avatar)
                })
            }
        })
    }

    async onLogoutPressed() {
        const { token } = this.props.auth
        const { onLogout } = this.props
        if (token) {
            onLogout(token)
        }
    }

    onPressDocument() {
        const { navigation } = this.props
        navigation.navigate('DocumentNav')
    }

    componentDidUpdate(prevProps: Props) {
        const { error } = this.props
        if (error && prevProps.error !== error) {
            setTimeout(() => {
                Alert.alert(error)
            }, 300)
        }
    }

}

const mapStateToProps = (state: RootState): StateProps => ({
    ...state.user,
    auth: state.auth
})

const mapDispatchToProps = (dispatch: any): DispatchProps => {
    return {
        onAvatarChanged: (picture) => dispatch(updateUserAvatar({ picture: picture })),
        onLogout: (token) => dispatch(logout({ token: token })),
    }
}

const ProfileContainer = connect(mapStateToProps, mapDispatchToProps)(Profile)

export default ProfileContainer
