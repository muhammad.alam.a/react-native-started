import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { BurgerButton } from "../_components/headers/buttons/BurgerButton";
import { MainNavParamList } from "../Main";
import { HeaderLeftView } from "../_components/headers/HeaderLeftView";
import ProfileContainer from "./Profile";

const Nav = createStackNavigator<MainNavParamList>()

const ProfileNav: React.FC = () => {
    return (
        <Nav.Navigator screenOptions={{ headerTransparent: true, headerTintColor: 'white' }}>
            <Nav.Screen name='Profile' component={ProfileContainer} />
        </Nav.Navigator>
    )
}

export default ProfileNav


/// Custom Navigation Header

export const ProfileNavDrawer: React.FC = () => {
    return (
        <Nav.Navigator screenOptions={
            ({ navigation }) => ({
                headerTransparent: true,
                headerTintColor: 'white',
                headerLeft: (props) => (
                    <HeaderLeftView>
                        <BurgerButton {...props} onPress={() => (navigation.toggleDrawer())} />
                    </HeaderLeftView>
                )
            })
        }>
            <Nav.Screen name='Profile' component={ProfileContainer} />
        </Nav.Navigator>
    )
}
