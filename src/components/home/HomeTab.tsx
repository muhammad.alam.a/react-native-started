import React from 'react'
import { createBottomTabNavigator, BottomTabBarOptions } from '@react-navigation/bottom-tabs';
import PlacesNav from '../places/PlacesNav';
import ProfileNav from '../profile/ProfileNav';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { MainNavParamList } from '../Main';

type HomeTabParamList = Pick<MainNavParamList, 'Places' | 'Profile'>
const Tab = createBottomTabNavigator<HomeTabParamList>()

const HomeTab: React.FC = () => {
    return (
        <Tab.Navigator screenOptions={screenOptions} tabBarOptions={tabOptions}>
            <Tab.Screen name="Places" component={PlacesNav} />
            <Tab.Screen name="Profile" component={ProfileNav} />
        </Tab.Navigator>
    )
}

const tabIcons: TabIconList = ({
    Places: {
        focused: 'ios-information-circle',
        unfocused: 'ios-information-circle-outline'
    },
    Profile: {
        focused: 'ios-person',
        unfocused: 'ios-person'
    },
})

const tabOptions: BottomTabBarOptions = {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
}

export default HomeTab

/// TYPES

type TabIconProps = { focused: string, unfocused: string }
type TabIconList = Record<keyof HomeTabParamList, TabIconProps>

const OptionsType = Tab.Navigator.defaultProps?.screenOptions
const screenOptions: typeof OptionsType = ({ route }) => ({
    tabBarIcon: ({ focused, color, size }) => {
        const name = focused ? tabIcons[route.name].focused : tabIcons[route.name].unfocused
        return <Ionicons name={name} size={size} color={color} />
    }
})
