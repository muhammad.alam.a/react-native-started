import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import PlacesDetailNav from '../places/PlacesDetailNav';
import { MainNavParamList } from '../Main';
import HomeTab from './HomeTab';
import DocumentNav from '../document/DocumentNav';

const Nav = createStackNavigator<MainNavParamList>()

const HomeNav: React.FC = () => {
    return (
        <Nav.Navigator headerMode={'none'}>
            <Nav.Screen name='HomeTab' component={HomeTab} />
            <Nav.Screen name='PlacesDetailNav' component={PlacesDetailNav} />
            <Nav.Screen name='DocumentNav' component={DocumentNav} />
        </Nav.Navigator>
    )
}

export default HomeNav
