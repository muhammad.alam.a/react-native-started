import React from 'react'
import { createDrawerNavigator, DrawerContentOptions } from '@react-navigation/drawer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { MainNavParamList } from '../Main';
import { ProfileNavDrawer } from '../profile/ProfileNav';
import { ExploreNavDrawer } from '../explore/ExploreNav';

type HomeDrawerParamList = Pick<MainNavParamList, 'Explore' | 'Favorites' | 'Profile'>
const Drawer = createDrawerNavigator<HomeDrawerParamList>()

const HomeDrawer: React.FC = () => {
    return (
        <Drawer.Navigator drawerType={"front"} screenOptions={screenOptions} drawerContentOptions={drawerOptions}>
            <Drawer.Screen name="Explore" component={ExploreNavDrawer} />
            <Drawer.Screen name="Favorites" component={ExploreNavDrawer} />
            <Drawer.Screen name="Profile" component={ProfileNavDrawer} />
        </Drawer.Navigator>
    )
}

const drawerIcons: DrawerIconList = {
    Explore: {
        focused: 'ios-business',
        unfocused: 'ios-business'
    },
    Favorites: {
        focused: 'ios-heart',
        unfocused: 'ios-heart'
    },
    Profile: {
        focused: 'ios-person',
        unfocused: 'ios-person'
    },
}

const drawerOptions: DrawerContentOptions = {
    activeTintColor: 'tomato',
    inactiveTintColor: 'gray',
}

export default HomeDrawer

/// TYPES

type DrawerIconProps = { focused: string, unfocused: string }
type DrawerIconList = Record<keyof HomeDrawerParamList, DrawerIconProps>

const OptionsType = Drawer.Navigator.defaultProps?.screenOptions
const screenOptions: typeof OptionsType = ({ route }) => ({
    drawerIcon: ({ focused, color, size }) => {
        const name = focused ? drawerIcons[route.name].focused : drawerIcons[route.name].unfocused
        return <Ionicons name={name} size={size} color={color} />
    }
})
