import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { MainNavParamList } from '../Main';
import PlacesDetailNav from '../places/PlacesDetailNav';
import HomeDrawer from './HomeDrawer';
import DocumentNav from '../document/DocumentNav';

const Nav = createStackNavigator<MainNavParamList>()

const HomeDrawerNav: React.FC = () => {
    return (
        <Nav.Navigator headerMode={'none'}>
            <Nav.Screen name='HomeDrawer' component={HomeDrawer} />
            <Nav.Screen name='PlacesDetailNav' component={PlacesDetailNav} />
            <Nav.Screen name='DocumentNav' component={DocumentNav} />
        </Nav.Navigator>
    )
}

export default HomeDrawerNav
