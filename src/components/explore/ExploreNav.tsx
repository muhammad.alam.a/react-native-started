import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { BurgerButton } from "../_components/headers/buttons/BurgerButton";
import { HeaderBackgroundView } from "../_components/headers/HeaderBackgroundView";
import { TextButton } from "../_components/headers/buttons/TextButton";
import Animated from "react-native-reanimated";
import { BackButton } from "../_components/headers/buttons/BackButton";
import { HeaderLeftView } from "../_components/headers/HeaderLeftView";
import { HeaderRightView } from "../_components/headers/HeaderRightView";
import { MainNavParamList } from "../Main";
import PlacesDetailContainer from "../places/PlacesDetail";
import ExploreContainer from "./Explore";

const Nav = createStackNavigator<MainNavParamList>()

const ExploreNav: React.FC = () => {
    return (
        <Nav.Navigator screenOptions={{
            headerTintColor: 'white',
            headerTransparent: true,
        }}>
            <Nav.Screen name='Explore' component={ExploreContainer} />
        </Nav.Navigator>
    )
}

export default ExploreNav


/// Custom Navigation Header

export const ExploreNavDrawer: React.FC = () => {
    const headerFadeAnim = new Animated.Value(0)
    return (
        <Nav.Navigator screenOptions={
            ({ navigation, route }) => ({
                gestureEnabled: true,
                headerTransparent: true,
                headerTintColor: 'white',
                headerBackground: (props) => (
                    <Animated.View style={{ flex: 1, opacity: headerFadeAnim }}>
                        <HeaderBackgroundView style={{ backgroundColor: 'rgb(210,5,105)' }} />
                    </Animated.View>
                ),
                headerLeft: (props) => (
                    <HeaderLeftView>
                        {!props.canGoBack
                            ? <BurgerButton {...props} onPress={() => (navigation.toggleDrawer())} />
                            : <BackButton {...props} onPress={() => (navigation.goBack())} />
                        }
                    </HeaderLeftView>
                ),
                headerRight: (props) => (
                    <HeaderRightView>
                        <TextButton {...props} title={'Login'} onPress={() => (navigation.replace('Login'))} />
                    </HeaderRightView>
                ),
            })
        }>
            <Nav.Screen name='Explore' initialParams={{ judul: 'Eksplorasi' }} children={(props) => (<ExploreContainer {...props} headerFadeAnim={headerFadeAnim} />)} />
            <Nav.Screen name='PlacesDetail' component={PlacesDetailContainer} />
        </Nav.Navigator>
    )
}
