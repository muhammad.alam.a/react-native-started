import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { styles } from '../../../styles'
import { View, Text, Image } from 'react-native'
import Place from '../../../models/Place'

interface Props {
    data: Place
    onPress?: (data: Place) => void
}

const PlaceCell: React.FC<Props> = ({ data, onPress }) => {
    return (
        <TouchableOpacity style={styles.cell} onPress={() => (onPress ? onPress(data) : () => (null))}>
            <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.title}>{data.name}</Text>
                    <Text style={styles.subtitle}>Alamat: {data.address}</Text>
                    <Text style={styles.subtitle}>Kategori: {data.placeCategory?.name}</Text>
                    <Text style={styles.subtitle}>Dokumen: {data.placeDocumentsId ?? 'kosong'}</Text>
                    <Text style={styles.subtitle}>Tes: {data.aaaNoExistPild ?? 'noex'}</Text>
                    {/* <Text style={styles.subtitle}>{index.toString()}</Text> */}
                </View>
                <Image style={{ width: 80, height: 80 }} source={{ uri: data.placeCategory?.imageSmallSquare }} />
            </View>
        </TouchableOpacity>
    )
}

export default PlaceCell
