import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Button } from 'react-native'
import ProfileContainer from '../../profile/Profile'
import PlacesContainer from '../../places/Places'
import { MainNavParamList } from '../../Main'

const TemplateStack = createStackNavigator<MainNavParamList>()

const StackTemplate = ({ navigation }: any) => {
    return (
        <TemplateStack.Navigator
            initialRouteName='Places'
            screenOptions={{
                headerRight: () => (
                    <Button title='Profile' onPress={() => { navigation.navigate('Profile') }} />
                )
            }}>
            <TemplateStack.Screen name="Profile" component={ProfileContainer} />
            <TemplateStack.Screen name="Places" component={PlacesContainer} />
        </TemplateStack.Navigator>
    )
}

export default StackTemplate
