import React from 'react'
import ProfileContainer from '../../profile/Profile'
import PlacesContainer from '../../places/Places'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MainNavParamList } from '../../Main';

const TemplateTab = createBottomTabNavigator<MainNavParamList>()

const TabTemplate = () => {
    return (
        <TemplateTab.Navigator>
            <TemplateTab.Screen name="Profile" component={ProfileContainer} />
            <TemplateTab.Screen name="Places" component={PlacesContainer} />
        </TemplateTab.Navigator>
    )
}

export default TabTemplate
