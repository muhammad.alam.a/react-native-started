import React from "react";
import { connect } from 'react-redux'
import { RootState } from "../../stores/rootReducer";

interface OwnProps { }

interface StateProps extends RootState { }

interface DispatchProps {
    onRefresh: () => void
}

type Props = OwnProps & StateProps & DispatchProps

class ScreenTemplate extends React.Component<Props> {

    render() {
        const { auth, places } = this.props
        const { onRefresh } = this.props

        return (
            null
        )
    }

}

const mapStateToProps = (state: RootState): StateProps => state

const mapDispatchToProps = (dispatch: any): DispatchProps => ({
    onRefresh: () => dispatch(),
})

const ScreenTemplateContainer = connect(mapStateToProps, mapDispatchToProps)(ScreenTemplate)

export default ScreenTemplateContainer
