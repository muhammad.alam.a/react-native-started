import React from 'react'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import { connect } from 'react-redux'
import { RootState } from '../stores/rootReducer'
import { AuthState } from '../stores/auth/types'
import LoginContainer from './auth/Login'
import HomeNav from './home/HomeNav'
import { NavigationContainer } from '@react-navigation/native'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import HomeDrawerNav from './home-drawer/HomeDrawerNav'
import Place from '../models/Place'

export type MainNavParamList = {
    Login: undefined
    Home: undefined
    HomeTab: undefined
    HomeDrawer: undefined
    Places: undefined
    PlacesDetail: { place: Place }
    PlacesDetailNav: { place: Place }
    Explore: { judul: String }
    Favorites: undefined
    Profile: undefined
    Document: undefined
    DocumentNav: undefined
}
const MainNav = createStackNavigator<MainNavParamList>()

interface StateProps extends AuthState { }
type Props = StateProps

const Main: React.FC<Props> = ({ token }) => {
    return (
        <SafeAreaProvider>
            <NavigationContainer>
                <MainNav.Navigator headerMode={'none'} screenOptions={{ ...TransitionPresets.FadeFromBottomAndroid }}>
                    <>
                        {token == undefined
                            ? <MainNav.Screen name="Login" component={LoginContainer} />
                            : <MainNav.Screen name="Home" component={HomeNav} />
                        }
                        <MainNav.Screen name="HomeDrawer" component={HomeDrawerNav} />
                    </>
                </MainNav.Navigator>
            </NavigationContainer>
        </SafeAreaProvider>
    )
}

const mapStateToProps = (state: RootState): AuthState => state.auth

const MainContainer = connect(mapStateToProps)(Main)

export default MainContainer
