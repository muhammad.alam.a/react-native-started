import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import { BurgerButton } from "../_components/headers/buttons/BurgerButton";
import { HeaderBackgroundView } from "../_components/headers/HeaderBackgroundView";
import { TextButton } from "../_components/headers/buttons/TextButton";
import Animated from "react-native-reanimated";
import PlacesDetailContainer from "./PlacesDetail";
import { BackButton } from "../_components/headers/buttons/BackButton";
import { HeaderLeftView } from "../_components/headers/HeaderLeftView";
import { HeaderRightView } from "../_components/headers/HeaderRightView";
import { MainNavParamList } from "../Main";
import PlacesContainer from "./Places";

const Nav = createStackNavigator<MainNavParamList>()

const PlacesNav: React.FC = () => {
    return (
        <Nav.Navigator screenOptions={{
            headerTintColor: 'white',
            headerTransparent: true,
        }}>
            <Nav.Screen name='Places' component={PlacesContainer} />
        </Nav.Navigator>
    )
}

export default PlacesNav


/// Custom Navigation Header

export const PlacesNavDrawer: React.FC = () => {
    const headerFadeAnim = new Animated.Value(0)
    return (
        <Nav.Navigator screenOptions={
            ({ navigation, route }) => ({
                gestureEnabled: true,
                headerTransparent: true,
                headerTintColor: 'white',
                headerBackground: (props) => (
                    <Animated.View style={{ flex: 1, opacity: headerFadeAnim }}>
                        <HeaderBackgroundView style={{ backgroundColor: 'rgb(210,5,105)' }} />
                    </Animated.View>
                ),
                headerLeft: (props) => (
                    <HeaderLeftView>
                        {!props.canGoBack
                            ? <BurgerButton {...props} onPress={() => (navigation.toggleDrawer())} />
                            : <BackButton {...props} onPress={() => (navigation.goBack())} />
                        }
                    </HeaderLeftView>
                ),
                headerRight: (props) => (
                    <HeaderRightView>
                        <TextButton {...props} title={'Login'} onPress={() => (navigation.replace('Login'))} />
                    </HeaderRightView>
                ),
            })
        }>
            <Nav.Screen name='Places' children={(props) => (<PlacesContainer {...props} headerFadeAnim={headerFadeAnim} />)} />
            <Nav.Screen name='PlacesDetail' component={PlacesDetailContainer} />
        </Nav.Navigator>
    )
}
