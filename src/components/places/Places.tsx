import React, { Component } from "react";
import { connect } from 'react-redux'
import Place from "../../models/Place";
import { RootState, getPlaces } from "../../stores/rootReducer";
import { StackNavigationProp } from '@react-navigation/stack';
import { BgOvalLarge } from "../_components/BgOval";
import { SafeAreaView } from "react-native-safe-area-context";
import { SafeAreaHeaderConsumer } from "../_components/SafeAreaHeaderConsumer";
import Animated, { Easing } from "react-native-reanimated";
import ListView from "../_components/ListView";
import PlaceCell from "./components/PlaceCell";
import { MainNavParamList } from "../Main";
import { PlacesState } from "../../stores/places/types";
import { fetchPlaces } from "../../stores/places/actions";

interface OwnProps {
    navigation: StackNavigationProp<MainNavParamList>
    headerFadeAnim?: Animated.Value<number>
}
interface StateProps extends PlacesState { }
interface DispatchProps {
    onRefresh: () => void
    onRefreshNext: (page: number) => void
}
type Props = OwnProps & StateProps & DispatchProps

class Places extends Component<Props, { tes: number }> {

    faded?: boolean = undefined

    render() {
        const { data, page, status, error } = this.props
        const { onRefresh, onRefreshNext } = this.props

        const isLoading = status == "pending"
        return (
            <>
                <BgOvalLarge />
                <SafeAreaHeaderConsumer>
                    {value => (
                        <ListView
                            // contentInset={{ top: value.headerHeight - value.insets.top }}
                            listStyle={{ marginTop: value.headerHeight }}
                            data={data}
                            page={page}
                            isRefreshing={isLoading && page <= 1}
                            isRefreshingNext={isLoading && page > 1}
                            onRefresh={onRefresh}
                            onRefreshNext={onRefreshNext}
                            renderItem={({ index, item }) => (
                                <PlaceCell data={item} onPress={(data) => this.onPressPlaceCell(data)} />
                            )}
                            error={error}
                            onScroll={event => this.onScroll(event.nativeEvent.contentOffset)}
                        />
                    )}
                </SafeAreaHeaderConsumer>
            </>
        )
    }

    onPressPlaceCell(place: Place) {
        const { navigation } = this.props
        // Alert.alert('You tapped the button!')
        navigation.push('PlacesDetailNav', { place: place })
    }

    onScroll(offset: { x: number, y: number }) {
        this.shouldFade(offset.y)
    }

    shouldFade(offset: number) {
        const animation = this.props.headerFadeAnim
        if (!animation) return

        const threshold = 0;
        this.faded = this.faded === undefined ? true : this.faded
        if (offset <= threshold && !this.faded) {
            this.faded = true
            Animated.timing(animation, {
                toValue: 0,
                duration: 150,
                easing: Easing.ease
            }).start()
        } else if (offset > threshold && this.faded) {
            this.faded = false
            Animated.timing(animation, {
                toValue: 1,
                duration: 150,
                easing: Easing.ease
            }).start()
        }
    }
}

const mapStateToProps = (state: RootState, props: OwnProps): StateProps => getPlaces(state)

const mapDispatchToProps = (dispatch: any, props: OwnProps): DispatchProps => {
    return {
        onRefresh: () => dispatch(fetchPlaces({ page: 1 })),
        onRefreshNext: (page) => dispatch(fetchPlaces({ page: page }))
    }
}

const PlacesContainer = connect(mapStateToProps, mapDispatchToProps)(Places)

export default PlacesContainer
