import React from "react";
import { connect } from 'react-redux'
import { RootState } from "../../stores/rootReducer";
import { Text, View, Button } from "react-native";
import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { BgOvalSmall } from "../_components/BgOval";
import { MainNavParamList } from "../Main";
import Place from "../../models/Place";

interface OwnProps {
    route: RouteProp<MainNavParamList, 'PlacesDetail'>
    navigation: StackNavigationProp<MainNavParamList>
}
interface StateProps {
    place: Place
}
interface DispatchProps {
    onRefresh: () => void
}
type Props = OwnProps & StateProps & DispatchProps

class PlacesDetail extends React.Component<Props> {

    constructor(props: Props) {
        super(props)

        this.props.navigation.setOptions({
            title: 'Title'
        })
    }

    render() {
        const { place } = this.props
        const { navigation } = this.props

        return (
            <>
                <BgOvalSmall />
                <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                    <Text>{place.name}</Text>
                    <Button title='Back' onPress={() => (navigation.pop())} />
                    <Button title='Next' onPress={() => (navigation.push('PlacesDetail', { place: place }))} />
                </View>
            </>
        )
    }

}

const mapStateToProps = (state: RootState, ownProps: OwnProps): StateProps => ({
    place: ownProps.route.params.place
})

const mapDispatchToProps = (dispatch: any): DispatchProps => ({
    onRefresh: () => dispatch(),
})

const PlacesDetailContainer = connect(mapStateToProps, mapDispatchToProps)(PlacesDetail)

export default PlacesDetailContainer
