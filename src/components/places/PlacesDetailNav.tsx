import React from "react";
import { RouteProp } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { BackButton } from "../_components/headers/buttons/BackButton";
import { HeaderLeftView } from "../_components/headers/HeaderLeftView";
import { MainNavParamList } from "../Main";
import PlacesDetailContainer from "./PlacesDetail";

const Nav = createStackNavigator<MainNavParamList>()

interface NavProps {
    route: RouteProp<MainNavParamList, 'PlacesDetailNav'>
}

const PlacesDetailNav: React.FC<NavProps> = ({ route }) => {
    return (
        <Nav.Navigator screenOptions={
            ({ navigation }) => ({
                headerTransparent: true,
                headerTintColor: 'white',
                headerLeft: (props) => (
                    <HeaderLeftView>
                        <BackButton {...props} onPress={() => (navigation.goBack())} />
                    </HeaderLeftView>
                ),
            })
        }>
            <Nav.Screen name='PlacesDetail' component={PlacesDetailContainer} initialParams={{ place: route.params.place }} />
        </Nav.Navigator>
    )
}

export default PlacesDetailNav
