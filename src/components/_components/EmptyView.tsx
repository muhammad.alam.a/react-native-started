import React from "react";
import { View, Text, Button } from "react-native";

interface Props {
    error?: string
    onPressRefresh?: () => void
}

const EmptyView: React.FC<Props> = ({ error, onPressRefresh }) => {
    const errorView = error
        ? <Text>{error}</Text>
        : null

    return (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
            {errorView}
            <Text>No content</Text>
            {onPressRefresh
                ? <Button title='Refresh' onPress={onPressRefresh} />
                : null
            }
        </View>
    )
}

export default EmptyView
