import React from 'react'
import { View, ActivityIndicator } from 'react-native';

const LoadingView = () => (
    <View style={{ flex: 1, alignSelf: "center" }}>
        <ActivityIndicator />
    </View>
)

export default LoadingView
