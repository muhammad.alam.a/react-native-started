import React from 'react'
import { View, ViewStyle, StyleProp } from 'react-native'

interface Props {
    style?: StyleProp<ViewStyle>
}

export const HeaderBackgroundView: React.FC<Props> = ({ style }) => {
    return (
        <View
            style={[style, {
                flex: 1,
                shadowColor: 'grey',
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 1
            }]} />
    )
}
