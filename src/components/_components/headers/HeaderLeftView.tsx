import React from 'react'
import { View, StyleProp, ViewStyle } from 'react-native'

interface HeaderLeftProps {
    style?: StyleProp<ViewStyle>
}

export const HeaderLeftView: React.FC<HeaderLeftProps> = (props) => {
    return (
        <View style={[props.style, { marginLeft: 12 }]}>
            {props.children}
        </View>
    )
}
