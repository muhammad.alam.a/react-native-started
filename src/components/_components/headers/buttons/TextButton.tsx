import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Text } from 'react-native';
import { BarButtonProps } from "./Types";

interface Props extends BarButtonProps {
    title: string
}
export const TextButton: React.FC<Props> = (props) => {
    return (
        <TouchableOpacity
            activeOpacity={0.6}
            style={{ paddingLeft: 6, paddingRight: 6 }}
            onPress={props.onPress}>
            <Text style={{ color: props.tintColor, fontWeight: "500", fontSize: 16 }}>{props.title}</Text>
        </TouchableOpacity>
    )
}
