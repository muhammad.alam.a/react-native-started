export interface BarButtonProps {
    onPress: () => void
    tintColor?: string
}
