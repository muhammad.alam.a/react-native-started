import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Text } from 'react-native';
import { BarButtonProps } from './Types';

interface Props extends BarButtonProps {
    title?: String
}
export const BackButton: React.FC<Props> = ({ onPress, tintColor, title }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.6}
            style={{ paddingLeft: 6, paddingRight: 6, flexDirection: "row" }}
            onPress={onPress}>
            <Ionicons name={'ios-arrow-back'} size={32} color={tintColor} />
            <Text style={{ color: tintColor, marginLeft: 8, fontWeight: "500" }}>{title}</Text>
        </TouchableOpacity>
    )
}
