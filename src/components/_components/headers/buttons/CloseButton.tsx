import React from 'react'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { BarButtonProps } from './Types';

interface Props extends BarButtonProps { }
export const CloseButton: React.FC<Props> = ({ onPress, tintColor }) => {
    return (
        <TouchableOpacity
            activeOpacity={0.6}
            style={{ paddingLeft: 6, paddingRight: 6 }}
            onPress={onPress}>
            <Ionicons name={'ios-close'} size={44} color={tintColor} />
        </TouchableOpacity>
    )
}
