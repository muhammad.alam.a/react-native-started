import React from 'react'
import { View, StyleProp, ViewStyle } from 'react-native'

interface HeaderRightProps {
    style?: StyleProp<ViewStyle>
}

export const HeaderRightView: React.FC<HeaderRightProps> = (props) => {
    return (
        <View style={[props.style, { marginRight: 12 }]}>
            {props.children}
        </View>
    )
}
