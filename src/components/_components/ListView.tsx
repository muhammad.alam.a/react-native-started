import React from "react";
import { FlatList, View, ActivityIndicator, NativeScrollEvent, NativeSyntheticEvent, ListRenderItem, Insets, StyleProp, ViewStyle } from "react-native";
import EmptyView from "./EmptyView";

interface Props<T> {
    data: Array<T>
    page: number
    isRefreshing: boolean
    isRefreshingNext: boolean
    renderItem: ListRenderItem<T>
    error?: string
    onRefresh?: () => void
    onRefreshNext?: (page: number) => void
    onScroll?: (event: NativeSyntheticEvent<NativeScrollEvent>) => void
    contentInset?: Insets
    listStyle?: StyleProp<ViewStyle>
}

class ListView<T> extends React.Component<Props<T>> {

    render() {
        const { data, page, isRefreshing, isRefreshingNext, error } = this.props
        const { onRefresh, onRefreshNext, onScroll } = this.props
        const { renderItem } = this.props
        const { contentInset, listStyle } = this.props

        const shouldEmpty = data.length == 0 && !isRefreshing
        return (shouldEmpty
            ? <EmptyView error={error} onPressRefresh={onRefresh} />
            : <FlatList<T>
                style={listStyle}
                contentInset={contentInset}
                contentOffset={{ x: 0, y: -(contentInset?.top || 0) }}
                data={data}
                renderItem={renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshing={isRefreshing}
                onRefresh={onRefresh}
                onEndReached={onRefreshNext ? () => onRefreshNext(page + 1) : null}
                onEndReachedThreshold={0.2}
                ListFooterComponent={isRefreshingNext ? this.renderLoadMore() : null}
                onScroll={onScroll}
                scrollEventThrottle={16} />
        )
    }

    renderLoadMore() {
        return (
            <View style={{ margin: 16 }}>
                <ActivityIndicator size="small" />
            </View>
        )
    }

}

export default ListView
