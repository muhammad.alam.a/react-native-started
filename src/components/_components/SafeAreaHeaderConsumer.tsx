import React from 'react'
import { HeaderHeightContext } from '@react-navigation/stack'
import { SafeAreaConsumer, EdgeInsets } from 'react-native-safe-area-context'

interface ConsumerProps {
    children: (value: ConsumerType) => Element
}
type ConsumerType = {
    headerHeight: number
    insets: EdgeInsets
}
export const SafeAreaHeaderConsumer: React.FC<ConsumerProps> = (props) => {
    return (
        <HeaderHeightContext.Consumer>
            {headerHeight => (
                <SafeAreaConsumer>
                    {insets => (
                        props.children({
                            headerHeight: headerHeight || 0,
                            insets: insets || { top: 0, bottom: 0, left: 0, right: 0 }
                        })
                    )}
                </SafeAreaConsumer>
            )}
        </HeaderHeightContext.Consumer>
    )
}
