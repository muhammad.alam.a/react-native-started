import React from 'react'
import { Image } from 'react-native'
import images from '../../res/images'

export const BgOvalSmall = () => {
    return (
        <Image style={{ position: "absolute", width: '100%', aspectRatio: 360 / 140, height: undefined }} source={images.bg_oval_small} />
    )
}

export const BgOvalLarge = () => {
    return (
        <Image style={{ position: "absolute", width: '100%', aspectRatio: 360 / 208, height: undefined }} source={images.bg_oval_large} />
    )
}
