const images = {
  bg_oval_large: require('./images/bg_oval_large.png'),
  bg_oval_small: require('./images/bg_oval_small.png')
}

export default images
