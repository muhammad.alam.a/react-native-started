import PlaceCategory from './PlaceCategory';

export default interface Place {
    id: number
    name: string
    address: string
    placeDocumentsId: number
    placeCategory?: PlaceCategory
    aaaNoExistPild: string
}
