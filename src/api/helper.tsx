import RNFetchBlob, { FetchBlobResponse } from 'rn-fetch-blob'
import { UploadFile, ApiError, ApiErrorCode, ApiErrorList } from "./types";
import { Platform } from "react-native";
import camelcaseKeys from "camelcase-keys";
import { devLogError } from '../utilities/dev';

/// CLIENT WRAPPER

type Methods = "POST" | "GET" | "DELETE" | "PUT" | "post" | "get" | "delete" | "put";
interface IFetchOptions {
    params?: any,
    body?: any,
    headers?: {
        [key: string]: string;
    } | undefined,
}
interface IFetch {
    baseUrl: string,
    fetch: (method: Methods, url: string, options: IFetchOptions) => Promise<FetchBlobResponse>
}

export function createFetcher(config: { baseUrl: string }): IFetch {
    return {
        baseUrl: config.baseUrl,
        fetch: async (method, url, options) => {
            url = config.baseUrl + url
            const params = createParams(options.params) ?? ''

            if (process.env.NODE_ENV !== 'production') {
                console.log(`[${method.toUpperCase()}] ${url}`)
                console.log('params', options.params)
                console.log('body', options.body)
            }

            const request = RNFetchBlob.config({ timeout: 30000 })
                .fetch(method, url + params, options.headers, options.body)
            return request
                .then(response => {
                    const code = response.info().status
                    if (code == 200) {
                        return response
                    }
                    throw wentErrorAny(code, `Client error ${code}`)
                })
                .catch(error => {
                    devLogError(fetch, error)
                    throw humanError()
                })
        }
    }
}

export function createParams(body: any): string | undefined {
    if (body == undefined) return undefined
    let data: Array<string> = []
    Object.keys(body).forEach(key => {
        data.push(encodeURIComponent(key) + '=' + encodeURIComponent(body[key]))
    })
    return '?' + data.join('&')
}

export function createFormData(body: any) {
    let data: Array<any> = []

    Object.keys(body).forEach(key => {
        if (body[key] instanceof UploadFile) {
            const file: UploadFile = body[key]
            const uri = Platform.OS === 'android' ? file.uri : file.uri.replace('file://', '')
            data.push({
                name: key,
                filename: file.filename || 'filename',
                type: file.type || 'image/jpeg',
                data: RNFetchBlob.wrap(uri),
            })
        } else {
            data.push({
                name: key,
                data: body[key],
            })
        }
    })

    return data
}

/// DATA MAPPER

const humanError = (error?: ApiError) =>
    error?.code > 0
        ? error?.message || `Something went wrong`
        : `Something went wrong`
const wentError = (code: ApiErrorCode) => new ApiError(code, ApiErrorList[code])
const wentErrorAny = (code: any, message: any = undefined) => new ApiError(code, message)

const isSuccessCode = (code: number) => (code >= 200 && code <= 299)

export function getResponseJson(json: any) {
    try {
        if (json == undefined || json.status == undefined) {
            throw wentError('invalidResponse')
        }
    } catch (error) {
        devLogError(getResponseJson, error, json)
        throw error
    }
    if (isSuccessCode(json.status) == false) {
        devLogError(getResponseJson, json.status, json)
        throw wentErrorAny(json.status, json.message)
    }
    return camelcaseKeys(json, { deep: true })
}

export function getResultJson(responseJson: any) {
    try {
        // ignore result validation if status code is succes
        if (isSuccessCode(responseJson.status) == false && responseJson.result == undefined) {
            throw wentError('invalidResult')
        }
        return responseJson.result
    } catch (error) {
        devLogError(getResultJson, error, responseJson)
        throw error
    }
}

function parse<T>(json: any): T {
    const model: T = json
    return model
}

export function parseObject<T>(json: object): T {
    try {
        const model = parse<T>(json)
        return model
    } catch (error) {
        devLogError(parseObject, error, json)
        throw wentError("parsingFailed")
    }
}

export function parseObjects<T>(json: any): T[] {
    if (Object.prototype.toString.call(json) !== '[object Array]') return []
    try {
        const array = json as Array<object>
        const models = array.map(item => {
            const model = parse<T>(item)
            return model
        })
        return models
    } catch (error) {
        devLogError(parseObjects, error, json)
        throw wentError('parsingFailed')
    }
}

export function mapResult<T>(data: any): T {
    try {
        const responseJson = getResponseJson(data)
        const resultJson = getResultJson(responseJson)
        const result = parseObject<T>(resultJson)
        return result
    } catch (error) {
        throw humanError(error)
    }
}

export function mapResults<T>(data: any): T[] {
    try {
        const responseJson = getResponseJson(data)
        const resultJson = getResultJson(responseJson)
        const result = parseObjects<T>(resultJson)
        return result
    } catch (error) {
        throw humanError(error)
    }
}
