export class UploadFile {
    uri: string
    type?: string
    filename?: string

    constructor(uri: string, type?: string, filename?: string) {
        this.uri = uri
        this.type = type
        this.filename = filename
    }
}

export const ApiErrorList = {
    invalidResponse: 'invalid response',
    invalidResult: 'invalid result',
    parsingFailed: 'parsing failed',
    400: 'unauthorized'
}

export type ApiErrorCode = keyof typeof ApiErrorList

export class ApiError {
    readonly code: any
    readonly message: any

    constructor(code: any, message: any = undefined) {
        this.code = code
        this.message = message
    }
}
