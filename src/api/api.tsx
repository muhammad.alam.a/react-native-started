import * as api from "./client";
import Place from "../models/Place";
import Auth from "../models/Auth";
import { UploadFile } from "./types";
import User from "../models/User";

export const login = (email: string, password: string) =>
    api.postResult<Auth>('/user/login', { email: email, password: password })

export const logout = (token: string) =>
    api.postResult<any>('/user/logout', { token: token })

export const updateUserAvatar = (token: string, picture: UploadFile) =>
    api.postFormDataResult<User>('/user/profile', { token: token, picture: picture })

export const getPlaces = (page: number) =>
    api.getResults<Place>('/places', { page: page })
