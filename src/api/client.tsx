import { API_BASE } from '../settings'
import { createFetcher, createFormData, mapResult, mapResults } from './helper'

const client = createFetcher({ baseUrl: API_BASE })

export default client

export async function getResult<T>(url: string, params?: {}): Promise<T> {
    return client.fetch('GET', url, { params: params })
        .then(response => mapResult(response.json()))
}

export async function getResults<T>(url: string, params?: {}): Promise<T[]> {
    return client.fetch('GET', url, { params: params })
        .then(response => mapResults(response.json()))
}

export async function postResult<T>(url: string, params?: {}): Promise<T> {
    const formData = createFormData(params)
    return client.fetch('POST', url, { body: formData })
        .then(response => mapResult(response.json()))
}

export async function postFormDataResult<T>(url: string, params?: {}): Promise<T> {
    const formData = createFormData(params)
    return client.fetch('POST', url, { body: formData, headers: { 'Content-Type': 'multipart/form-data' } })
        .then(response => mapResult(response.json()))
}
