import AsyncStorage from "@react-native-community/async-storage"

async function setValue(key: string, value?: string): Promise<void> {
    try {
        if (value == undefined) {
            return await AsyncStorage.removeItem(key)
        }
        return await AsyncStorage.setItem(key, value)
    } catch (e) {
        return Promise.reject()
    }
}

async function getValue(key: string): Promise<string | undefined> {
    try {
        const value = await AsyncStorage.getItem(key)
        return value || undefined
    } catch (e) {
        return Promise.reject()
    }
}

export const setToken = (value?: string) => setValue('token', value)
export const getToken = () => getValue('token')
