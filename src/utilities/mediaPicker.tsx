import ImagePicker, { ImagePickerOptions, ImagePickerResponse } from 'react-native-image-picker'

const options: ImagePickerOptions = {
    title: 'Select image',
    allowsEditing: false,
    quality: 0.5,
    storageOptions: {
        skipBackup: true,
    },
}

export function showImagePicker(callback: (response: ImagePickerResponse) => void) {
    ImagePicker.showImagePicker({ ...options, ...{ mediaType: 'photo' } }, callback)
}
