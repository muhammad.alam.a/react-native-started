const info = 'This log only appear on dev mode.'

export const devLog = (title: any, message?: any, stack?: any) => {
    if (isDev()) {
        title = title.name || title
        if (message == undefined && stack == undefined) {
            console.log(info, title)
        } else if (stack == undefined) {
            console.log(info, title, message)
        } else {
            console.log(info, title, message, stack)
        }
    }
}

export const devLogError = (title: any, message?: any, stack?: any) => {
    if (isDev()) {
        title = title.name || title
        if (message == undefined && stack == undefined) {
            console.warn(info, title)
        } else if (stack == undefined) {
            console.warn(info, title, message)
        } else {
            console.warn(info, title, message, stack)
        }
    }
}

export const isDev = () => {
    return process.env.NODE_ENV !== 'production'
}
