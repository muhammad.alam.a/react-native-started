import { StatusType } from "../types";

export interface AuthState {
    token?: string
    status: StatusType
    error?: string
    currentRequestId: any,
}
