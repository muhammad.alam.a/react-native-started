import { createAsyncThunk, createAction } from "@reduxjs/toolkit";
import * as api from "../../api/api";
import { RootState } from "../rootReducer";

export const login = createAsyncThunk(
    'auth/login',
    (arg: { email: string, password: string }, thunkApi) => api.login(arg.email, arg.password),
    {
        condition: ({ }, { getState }) => {
            const { auth } = getState() as RootState
            return (auth.status == 'idle')
        }
    }
)

export const logout = createAsyncThunk(
    'auth/logout',
    (arg: { token: string }, { dispatch }) => api.logout(arg.token).finally(() => dispatch(clearAuth())),
    {
        condition: ({ }, { getState }) => {
            const { auth } = getState() as RootState
            return (auth.status == 'idle')
        }
    }
)

export const clearAuth = createAction('clearAuth')
