import { createSlice } from "@reduxjs/toolkit";
import { AuthState } from "./types";
import { login, logout, clearAuth } from "./actions";

export const initialState: AuthState = {
    token: undefined,
    status: 'idle',
    error: undefined,
    currentRequestId: undefined,
}

const authSlice = createSlice({
    name: 'auth',
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(clearAuth, (state, action) => {
            return initialState
        })

        // login reducers
        builder.addCase(login.pending, (state, action) => {
            if (state.status == 'idle') {
                state.status = "pending"
                state.error = undefined
                state.currentRequestId = action.meta.requestId
            }
        })
        builder.addCase(login.fulfilled, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                state.token = action.payload.token
                state.status = "idle"
                state.currentRequestId = undefined
            }
        })
        builder.addCase(login.rejected, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                state.status = "idle"
                state.error = action.error.message
                state.currentRequestId = undefined
            }
        })

        // logout reducers
        builder.addCase(logout.pending, (state, action) => {
            if (state.status == 'idle') {
                state.status = "pending"
                state.error = undefined
                state.currentRequestId = action.meta.requestId
            }
        })
        // loogut fullfilled & rejected are ignored because we assure to dispatch clearAuth action.
    }
})

export const { } = authSlice.actions

const auth = authSlice.reducer
export default auth
