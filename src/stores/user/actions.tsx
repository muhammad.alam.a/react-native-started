import { createAsyncThunk } from "@reduxjs/toolkit";
import * as api from "../../api/api";
import { UploadFile } from "../../api/types";
import { RootState } from "../rootReducer";

export const updateUserAvatar = createAsyncThunk(
    'user/updateAvatar',
    (arg: { picture: UploadFile }, { getState }) => {
        const { auth } = getState() as RootState
        return api.updateUserAvatar(auth.token!, arg.picture)
    },
    {
        condition: (picture, { getState }) => {
            const { auth, user } = getState() as RootState
            return (user.status == 'idle' && auth?.token != undefined)
        }
    }
)
