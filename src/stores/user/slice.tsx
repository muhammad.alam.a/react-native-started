import { createSlice } from "@reduxjs/toolkit";
import { UserState } from "./types";
import { updateUserAvatar } from "./actions";
import { login, clearAuth } from "../auth/actions";

const initialState: UserState = {
    user: undefined,
    status: 'idle',
    error: undefined,
    currentRequestId: undefined
}

const userSlice = createSlice({
    name: 'user',
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(login.fulfilled, (state, action) => {
            state.user = action.payload.user
        })
        builder.addCase(clearAuth, (state, action) => {
            return initialState
        })

        // updateUser reducers
        builder.addCase(updateUserAvatar.pending, (state, action) => {
            if (state.status == 'idle') {
                state.status = "pending"
                state.error = undefined
                state.currentRequestId = action.meta.requestId
            }
        })
        builder.addCase(updateUserAvatar.fulfilled, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                state.user = action.payload
                state.status = "idle"
                state.currentRequestId = undefined
            }
        })
        builder.addCase(updateUserAvatar.rejected, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                state.status = "idle"
                state.error = action.error.message
                state.currentRequestId = undefined
            }
        })
    }
})

export const { } = userSlice.actions;

const user = userSlice.reducer
export default user
