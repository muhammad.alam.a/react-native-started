import { StatusType } from "../types";
import User from "../../models/User";

export interface UserState {
    user?: User
    status: StatusType
    error?: string
    currentRequestId: any
}
