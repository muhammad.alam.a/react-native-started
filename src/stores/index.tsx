import { configureStore, Action, getDefaultMiddleware } from "@reduxjs/toolkit"
import { ThunkAction } from 'redux-thunk'
import rootReducer, { RootState } from "./rootReducer";
import logger from "redux-logger";
import { persistStore, persistReducer, PersistConfig } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'
import hardSet from "redux-persist/es/stateReconciler/hardSet";
import { isDev } from "../utilities/dev";
import { PERSISTED_VERSION } from "../settings";

const middleware = [...getDefaultMiddleware({
    serializableCheck: false,
    immutableCheck: false
}), logger]

const preloadedState: RootState = {
    auth: {
        token: undefined,
        status: 'idle',
        error: undefined,
        currentRequestId: undefined
    },
    user: {
        user: undefined,
        status: 'idle',
        error: undefined,
        currentRequestId: undefined
    },
    places: {
        data: [],
        page: 0,
        status: 'idle',
        error: undefined,
        currentRequestId: undefined
    }
}

const persistConfig: PersistConfig<any> = {
    key: 'root',
    storage: AsyncStorage,
    version: PERSISTED_VERSION,
    stateReconciler: hardSet,
    migrate: (oldState): any => {
        return oldState?._persist.version === PERSISTED_VERSION
            ? Promise.resolve(oldState)
            : Promise.resolve(preloadedState)
    }
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const store = configureStore({
    reducer: persistedReducer,
    middleware,
    devTools: isDev(),
    preloadedState
})

export const persistor = persistStore(store)

export type AppDispatch = typeof store.dispatch
export type AppThunk = ThunkAction<void, RootState, null, Action<string>>
