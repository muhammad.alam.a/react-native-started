import Place from "../../models/Place"
import { LoadPlacesAction } from "./types"
import * as api from "../../api/api";

// export const newLoadPlacesRequest = createAction('LOAD_PLACES_REQUEST')

export const loadPlacesRequest = (page: number): LoadPlacesAction => ({
    type: "LOAD_PLACES_REQUEST",
    page
})

export const loadPlacesSuccess = (places: Place[], page: number): LoadPlacesAction => ({
    type: "LOAD_PLACES_SUCCESS",
    places,
    page
})

export const loadPlacesFailure = (error: string): LoadPlacesAction => ({
    type: "LOAD_PLACES_FAILURE",
    error
})

export const fetchPlaces = (page: number) => ((dispatch: any, getState: any) => {
    dispatch(loadPlacesRequest(page))
    return api.getPlaces(page)
        .then(response => dispatch(loadPlacesSuccess(response, page)))
        .catch(error => dispatch(loadPlacesFailure(error)))
})
