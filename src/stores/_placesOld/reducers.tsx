import Place from '../../models/Place'
import { combineReducers } from 'redux'
import { LoadPlacesAction, PlacesState } from './types'

const data = (state: Place[] = [], action: LoadPlacesAction) => {
    switch (action.type) {
        case 'LOAD_PLACES_SUCCESS':
            if (action.page <= 1)
                return [...action.places]
            else
                return [...state, ...action.places]
        default:
            return state
    }
}

const page = (state = 0, action: LoadPlacesAction) => {
    switch (action.type) {
        case 'LOAD_PLACES_REQUEST':
        case 'LOAD_PLACES_SUCCESS':
            return action.page
        default:
            return state
    }
}

const isLoading = (state = false, action: LoadPlacesAction) => {
    switch (action.type) {
        case 'LOAD_PLACES_REQUEST':
            return true
        case 'LOAD_PLACES_SUCCESS':
        case 'LOAD_PLACES_FAILURE':
            return false
        default:
            return state
    }
}

const error = (state: string | undefined, action: LoadPlacesAction) => {
    switch (action.type) {
        case 'LOAD_PLACES_FAILURE':
            return action.error
        case 'LOAD_PLACES_REQUEST':
        case 'LOAD_PLACES_SUCCESS':
            return undefined
        default:
            return state
    }
}

const places = combineReducers<PlacesState>({
    data,
    page,
    isLoading,
    error
})

export default places

export const getIsLoading = (state: PlacesState) => state.isLoading
export const getErrorMessage = (state: PlacesState) => state.error
