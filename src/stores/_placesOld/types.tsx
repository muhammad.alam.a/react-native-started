import Place from "../../models/Place";

export type LoadPlacesAction =
    | { type: 'LOAD_PLACES_REQUEST'; page: number }
    | { type: 'LOAD_PLACES_SUCCESS'; places: Place[]; page: number }
    | { type: 'LOAD_PLACES_FAILURE'; error: string }

export interface PlacesState {
    data: Place[]
    page: number
    isLoading: boolean
    error?: string
}
