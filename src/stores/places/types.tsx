import Place from "../../models/Place";
import { StatusType } from "../types";

export interface PlacesState {
    data: Place[]
    page: number
    status: StatusType
    error?: string
    currentRequestId: any
}
