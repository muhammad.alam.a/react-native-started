import { createSlice } from "@reduxjs/toolkit";
import { PlacesState } from "./types";
import { fetchPlaces } from "./actions";

const initialState: PlacesState = {
    data: [],
    page: 0,
    status: 'idle',
    error: undefined,
    currentRequestId: undefined
}

const placesSlice = createSlice({
    name: 'places',
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        // places reducers
        builder.addCase(fetchPlaces.pending, (state, action) => {
            const { page } = action.meta.arg
            if (state.status == 'idle' || page == 1) {
                state.page = page
                state.status = "pending"
                state.error = undefined
                state.currentRequestId = action.meta.requestId
            }
        })
        builder.addCase(fetchPlaces.fulfilled, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                if (state.page <= 1)
                    state.data = action.payload
                else
                    state.data = [...state.data, ...action.payload]
                state.status = "idle"
                state.currentRequestId = undefined
            }
        })
        builder.addCase(fetchPlaces.rejected, (state, action) => {
            const { requestId } = action.meta
            if (state.status == 'pending' && state.currentRequestId === requestId) {
                state.status = "idle"
                state.error = action.error.message
                state.currentRequestId = undefined
            }
        })
    }
})

export const { ...action } = placesSlice.actions;

const places = placesSlice.reducer
export default places
