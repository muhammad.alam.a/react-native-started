import * as api from "../../api/api";
import { createAsyncThunk } from "@reduxjs/toolkit";
import { RootState } from "../rootReducer";

export const fetchPlaces = createAsyncThunk(
    'places/fetch',
    (arg: { page: number }, thunkApi) => api.getPlaces(arg.page),
    {
        condition: ({ page }, { getState }) => {
            const { places } = getState() as RootState
            return (places.status == 'idle' || page == 1)
        }
    }
)
