import { combineReducers } from "redux";
import places from "./places/slice";
import auth from "./auth/slice";
import user from "./user/slice";

const rootReducer = combineReducers({
    auth,
    user,
    places
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer

export const getAuth = (state: RootState) => state.auth
export const getPlaces = (state: RootState) => state.places
