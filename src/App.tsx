import React, { Component } from "react";
import Main from "./components/App";
import { store, persistor } from "./stores";
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {

  componentDidMount() {
    SplashScreen.hide()
  }

  render() {
    return (
      <Main store={store} persistor={persistor} />
    )
  }

}
