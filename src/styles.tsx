import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    title: {
        color: 'black',
        fontSize: 14
    },
    subtitle: {
        color: '#444444',
        fontSize: 12
    },
    cell: {
        backgroundColor: 'white',
        borderRadius: 4,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        margin: 4,
        padding: 12,
        justifyContent: "center",
    },
});
